<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\PedidoOracao;


class LogOracao extends Model
{
    protected $fillable = [
        'pedido_id',
        'usuario_id'
    ];

    public function pedidoOracao(){
        return $this->belongsTo(PedidoOracao::class);
  }
}
