<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Conta extends Model
{
    protected $fillable = [
        'nome',
        'logo',
        'cep',
        'endereco',
        'bairro',
        'cidade',
        'estado',
        'pais',
        'cpf_cnpj',
        'inscricao_estadual',
        'razao_social',
        'nome_fantasia',
        'nome_responsavel',
        'sobrenome_resposavel',
        'telefone_responsavel',
        'email_responsavel'
    ];
}
