<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Destaque extends Model
{
    protected $fillable = [
        'titulo',
        'msg',
        'link'
    ];
}
