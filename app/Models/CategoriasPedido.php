<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CategoriasPedido extends Model
{
    protected $fillable = [ 'label' ];
    
}
