<?php

namespace App\Models;

class FilterData
{
    public static function Operators($operator)
    {
        switch ($operator) {
            case 'greater':
                return '>';
                break;

            case 'greater_equal':
                return '>=';
                break;

            case 'equal':
                return '=';
                break;

            case 'less':
                return '<';
                break;

            case 'less_equal':
                return '<=';
                break;

            case 'different':
                return '<>';
                break;

            case 'like':
                return 'like';
                break;

            default:
                return '';
                break;
        }
    }
}
