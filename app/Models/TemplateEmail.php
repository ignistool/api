<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class TemplateEmail extends Model
{
    protected $fillable = [ 'nome', 'remetente', 'assunto', 'corpo' ];

    protected $casts = [
        'created_at' => 'date:Y-m-d'
    ];
}
