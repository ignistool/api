<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class TiposUsuario extends Model
{
    protected $fillable = [ 'label' ];
}
