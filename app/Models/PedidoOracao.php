<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\LogOracao;

class PedidoOracao extends Model
{
    protected $fillable = [
        'nome_solicitante',
        'sobrenome_solicitante',
        'telefone_solicitante',
        'email_solicitante',
        'categoria_pedido_id',
        'texto',
        'origem_publico',
        'publico',
        'privado',
        'urgente',
        'enviado_plantao',
        'plantao_id',
        'retorno',
        'triado'
    ];

    protected $casts = [
        'origem_publico' => 'boolean',
        'publico' => 'boolean',
        'privado' => 'boolean',
        'urgente' => 'boolean',
        'created_at'  => 'date:d/m/Y H:i',
    ];

    public function logOracao()
    {
        return $this->hasMany(LogOracao::class, 'pedido_id');
    }
}
