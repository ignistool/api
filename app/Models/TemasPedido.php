<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class TemasPedido extends Model
{
    protected $fillable = [ 'label' ];
}
