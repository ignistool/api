<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Devocional extends Model
{
    protected $fillable = [
        'titulo',
        'texto',
        'citacao',
        'editor_id',
    ];
}
