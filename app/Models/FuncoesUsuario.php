<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class FuncoesUsuario extends Model
{
    protected $fillable = [ 'nome',  'acesso_paginas', 'acesso_funcoes' ];
}
