<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\FuncoesUsuario;
use Illuminate\Support\Facades\DB;


class FuncoesUsuarioController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return FuncoesUsuario::orderBy('id', 'desc')->get();
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $nome = $request->input('nome');
        $pag = $request->input('pagina');
        $func = $request->input('funcoes');

        $pags = array();
        $funcs = array();

        foreach ($pag as $value) {
            array_push($pags, $value);
        }
        foreach ($func as $value) {
            array_push($funcs, $value);
        }

        $funcao = DB::table('funcoes_usuarios')->insert([
            'nome' => $nome,
            'acesso_paginas' => json_encode($pags),
            'acesso_funcoes' => json_encode($funcs)
        ]);

        return response()->json($funcao, 201);

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return FuncoesUsuario::findOrfail($id);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        // $funcao = FuncoesUsuario::findOrfail($id);
        // $funcao->update($request->all());
        // return response()->json($funcao, 201);

        $nome = $request->input('nome');
        $pag = $request->input('pagina');
        $func = $request->input('funcoes');

        $pags = array();
        $funcs = array();

        foreach ($pag as $value) {
            array_push($pags, $value);
        }
        foreach ($func as $value) {
            array_push($funcs, $value);
        }

        $funcao = DB::table('funcoes_usuarios')->where('id', $id )->update([
            'nome' => $nome,
            'acesso_paginas' => json_encode($pags),
            'acesso_funcoes' => json_encode($funcs)
        ]);

        return response()->json($funcao, 201);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $funcao =  FuncoesUsuario::findOrfail($id);
        $funcao->delete();
    }
}
