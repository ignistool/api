<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class Usuario extends Controller
{
    public  function registra(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'email'=>'required|email',
            'nome'=>'required',
            'sobrenome'=>'required',
            'conta_id'=>'required',
            'tipo_usuario'=>'required',
            'funcao_usuario'=>'required',
            'password'=> 'required',           
        ]);


        $user = User::create(array_merge(
            $validator->validated(),
            ['password' => bcrypt($request->password)]
        ));

        return response()->json([
            'message' => 'Successfully registered',
            'user' => $user
        ], 201);
    }
}
