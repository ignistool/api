<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\PedidoOracao;
use App\Models\LogOracao;
use Illuminate\Support\Facades\DB;
use App\Models\FilterData as FilterData;



class PedidoOracaoController extends Controller
{

    public function orar(Request $request, $pedidoID, $userID)
    {
        $log = DB::table('log_oracaos')->insert([
            ['pedido_id' => $pedidoID, 'usuario_id' => $userID],
        ]);
        return response()->json($log, 201);
    }

    public function intercessao(Request $request)
    {
        $where = [
            [
                'field' => 'privado',
                'operator' => '=',
                'value' => 1
            ]
        ];

        if ($filters = $request->input('filter')) {
            foreach ($filters as $filter_json) {
                array_push($where, [
                    'field' => json_decode($filter_json)->field,
                    'operator' => FilterData::Operators(json_decode($filter_json)->operator),
                    'value' => json_decode($filter_json)->value
                ]);
            }
        }

        $pedidos = PedidoOracao::with('logOracao')
            ->where($where)
            ->orderBy('created_at', 'desc')
            ->paginate(10);


        return $pedidos;
    }

    public function triagem(Request $request)
    {
        $where = [
            [
                'field' => 'triado',
                'operator' => '<>',
                'value' => 1
            ]
        ];

        $pagination = 10;

        if ($filters = $request->input('filter')) {
            foreach ($filters as $filter_json) {
                array_push($where, [
                    'field' => json_decode($filter_json)->field,
                    'operator' => FilterData::Operators(json_decode($filter_json)->operator),
                    'value' => json_decode($filter_json)->value
                ]);
            }
        }

        if ($per_page = $request->input('per_page')) {
            $pagination = $per_page;
        }

        if ($request->input('type_query') == 'count') {
            return PedidoOracao::where($where)->count();
        }

        $pedidos = PedidoOracao::where($where)->orderBy('created_at', 'desc')->paginate($pagination);

        return $pedidos;
    }

    public function getPedidosSeed()
    {
        $pedidos = PedidoOracao::orderBy('created_at', 'desc')->get();
        $seeds = '';
        foreach ($pedidos as $pedido) {
            $seeds .= "[ 'id'=> 2, 'nome_solicitante'=> '" . $pedido->nome_solicitante . "', 'sobrenome_solicitante'=> '" . $pedido->sobrenome_solicitante . "', 'telefone_solicitante'=> '" . $pedido->telefone_solicitante . "', 'email_solicitante'=> '" . $pedido->email_solicitante . "', 'categoria_pedido_id'=> " . $pedido->categoria_pedido_id . ", 'texto'=> '" . $pedido->texto . "', 'origem_publico'=> " . $pedido->origem_publico . ",    'publico'=> " . $pedido->publico . ", 'privado'=> " . $pedido->privado . ", 'urgente'=> " . $pedido->urgente . ", 'enviado_plantao'=> " . $pedido->enviado_plantao . ",  'plantao_id'=> " . $pedido->plantao_id . ", 'retorno'=> " . $pedido->retorno . ", 'triado'=> " . $pedido->triado . ",    'created_at'=> '" . $pedido->created_at . "' ],";
        }

        return $seeds;
    }

    public function getReport2Months(Request $request)
    {

        $where = [];

        array_push($where, [
            'created_at', '>=', date('Y-m-', strtotime('-1 months', strtotime(date('Y-m-d')))) . '01'
        ]);
        array_push($where, [
            'created_at', '<=', date('Y-m-d')
        ]);

        $db = PedidoOracao::select([
            // This aggregates the data and makes available a 'count' attribute
            DB::raw('count(id) as `total`'),
            // This throws away the timestamp portion of the date
            DB::raw('DATE(created_at) as data')
            // Group these records according to that day
        ])
            ->where($where)
            ->orderBy('data', 'asc')

            ->groupBy('data')
            // And restrict these results to only those created in the last week
            //   ->where('created_at', '>=', Carbon\Carbon::now()->subWeeks(1))
            ->get();


        return $db;
    }


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $where = [];
        $pagination = 10;

        if ($filters = $request->input('filter')) {
            foreach ($filters as $filter_json) {
                array_push($where, [
                    'field' => json_decode($filter_json)->field,
                    'operator' => FilterData::Operators(json_decode($filter_json)->operator),
                    'value' => json_decode($filter_json)->value
                ]);
            }
        }

        if ($per_page = $request->input('per_page')) {
            $pagination = $per_page;
        }

        if ($request->input('type_query') == 'count') {
            return PedidoOracao::where($where)->count();
        }

        return PedidoOracao::where($where)->orderBy('created_at', 'desc')->paginate($pagination);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $pedido = PedidoOracao::create($request->all());
        return response()->json($pedido, 201);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return PedidoOracao::findOrfail($id);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $pedido = PedidoOracao::findOrfail($id);
        $pedido->update($request->all());
        return response()->json($pedido, 201);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $pedido =  PedidoOracao::findOrfail($id);
        $pedido->delete();
    }
}
