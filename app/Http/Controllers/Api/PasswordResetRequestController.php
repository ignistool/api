<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;
use App\Mail\SendMail;
use Illuminate\Support\Facades\Mail;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Hash;
use App\User;



class PasswordResetRequestController extends Controller
{

    public function sendPasswordResetEmail(Request $request)
    {
        // If email does not exist
        if (!$this->validEmail($request->email)) {
            return response()->json([
                'message' => 'E-mail informado não existe'
            ], 401);
        } else {

            echo '<pre>';
            var_dump($_SERVER);
            echo '</pre>';
            die();

            if ($_SERVER['HTTP_HOST'] == 'api-ignistool.test') {
                $urlBase = 'http://localhost:8081';
            } else {
                $urlBase = 'https://app.ignistool.com';
            }

            $this->sendMail($request->email, $urlBase);
            return response()->json([
                'message' => 'Confira sua caixa de entrada. Enviamos um e-mail para você recuperar sua senha.'
            ], Response::HTTP_OK);
        }
    }


    public function sendMail($email, $url)
    {
        $token = $this->generateToken($email);
        // Mail::to($email)->send(new SendMail($token));
        Mail::send('Email.forgot', ['token' => $token, 'urlBase' => $url], function ($message) use ($email) {

            $message->to($email);
            $message->subject('Recuperação de senha');
        });
    }

    public function validEmail($email)
    {
        return !!User::where('email', $email)->first();
    }

    public function generateToken($email)
    {

        $token = Str::random(80);

        $affected = DB::table('users')
            ->where('email', $email)
            ->update(['token_recover_password' => $token]);

        return $token;
    }

    public function storeToken($token, $email)
    {
        DB::table('recover_password')->insert([
            'email' => $email,
            'token' => $token,
            'created' => Carbon::now()
        ]);
    }

    public function ressetPassword(Request $request)
    {
        $token = $request->input('token');

        if (!$user = User::where('token_recover_password', $token)->first()) {
            return response([
                'message' => 'Usuario não existe!'

            ], 404);
        }

        $user->password = bcrypt($request->input('password'));
        $user->save();

        return response([
            'message' => 'Sua nova senha foi gerada com sucesso!'

        ], 201);
    }

    public function changePasswordInDashboard(Request $request)
    {
        $pasword = $request->input('password');
        $userID = $request->input('user_id');
        $new_pasword = bcrypt($request->input('new_pasword'));

        var_dump($pasword);

        if (!$user = User::where([
            ['id', $userID],
            // [ 'password', bcrypt($pasword) ],
        ])->first()) {
            return response([
                'message' => 'Usuario não existe!'

            ], 404);
        }

        // $user->password = $new_pasword;
        $user->password = bcrypt($new_pasword);
        $user->save();

        return response([
            'message' => 'Sua nova senha foi gerada com sucesso!'

        ], 201);
    }

    public function sendEmailPassword(Request $request)
    {
        // If email does not exist
        if (!$this->validEmail($request->email)) {
            return response()->json([
                'message' => 'O usuário selecionado não existe'
            ], 401);
        } else {
            // If email exists
            $this->sendMail($request->email);
            return response()->json([
                'message' => 'Foi enviado um link de alteração de senha para o endereço de e-mail do usuário.'
            ], Response::HTTP_OK);
        }
    }
}
