<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use App\Models\Conta;



class ContaController extends Controller
{

    public function getLogo(Request $request){
        $conta = DB::table('contas')
        ->where('id','1')
        ->select('logo')
        ->first();

        return response()->json($conta, 200);
    }

    public function changeLogo(Request $request)
    {
        $id = 1;

        if ($request->hasFile('file')) {
            // Pega o nome do arquivo com a extensão
            $filenameWithExt = $request->file('file')->getClientOriginalName();
            // Pega somente o nome do arquivo
            $filename = pathinfo($filenameWithExt, PATHINFO_FILENAME);
            // Pega somente a extensão
            $extension = $request->file('file')->getClientOriginalExtension();
            // Filename to store
            $fileNameToStoreWithSpace = $filename . '_' . time() . '.' . $extension;
            $fileNameToStore = str_replace(' ', '_', $fileNameToStoreWithSpace);
            // Upload do arquivo
            $path = $request->file('file')->storeAs('public/logo', $fileNameToStore);

            $request->merge([
                'logo' => $fileNameToStore
            ]);
        } else {
            $fileNameToStore = 'noimage1.png';

            $conta = Conta::findOrfail($id);
            if ($conta->file == null) {
                $request->merge([
                    'logo' => $fileNameToStore
                ]);
            }
        }

        // //save in database
        $conta = Conta::findOrfail($id);
        $conta->update($request->all());

        // trata url pública do avatar
        $conta->logo = $_SERVER['REQUEST_SCHEME'] . '://' . $_SERVER['SERVER_NAME'] . '/storage/logo/' . $conta->logo;

        return response()->json($conta, 200);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return Conta::all();
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $conta = Conta::create($request->all());
        return response()->json($conta, 201);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return Conta::findOrfail($id);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $conta = Conta::findOrfail($id);
        $conta->update($request->all());
        return response()->json($conta, 201);

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $conta =  Conta::findOrfail($id);
        $conta->delete();
    }
}
