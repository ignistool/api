<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\User;
use Illuminate\Support\Facades\DB;


class UserController extends Controller
{

    public function changeAvatar(Request $request, $id)
    {

        if ($request->hasFile('file')) {
            // Pega o nome do arquivo com a extensão
            $filenameWithExt = $request->file('file')->getClientOriginalName();
            // Pega somente o nome do arquivo
            $filename = pathinfo($filenameWithExt, PATHINFO_FILENAME);
            // Pega somente a extensão
            $extension = $request->file('file')->getClientOriginalExtension();
            // Filename to store
            $fileNameToStoreWithSpace = $filename . '_' . time() . '.' . $extension;
            $fileNameToStore = str_replace(' ', '_', $fileNameToStoreWithSpace);
            // Upload do arquivo
            $path = $request->file('file')->storeAs('public/avatar', $fileNameToStore);

            $request->merge([
                'avatar' => $fileNameToStore
            ]);
        } else {
            $fileNameToStore = 'noimage1.png';

            $user = User::findOrfail($id);
            if ($user->file == null) {
                $request->merge([
                    'avatar' => $fileNameToStore
                ]);
            }
        }

        // //save in database
        $user = User::findOrfail($id);
        $user->update($request->all());

        // trata url pública do avatar
        $user->avatar = $_SERVER['REQUEST_SCHEME'] . '://' . $_SERVER['SERVER_NAME'] . '/storage/avatar/' . $user->avatar;

        return response()->json($user, 200);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return User::all();
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $user = User::create($request->all());
        return response()->json($user, 201);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $user = User::findOrfail($id);
        if ($user->avatar != null) {
            $user->avatar = 'http://127.0.0.1/storage/avatar/' . $user->avatar;
        }

        return $user;
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $user = User::findOrfail($id);
        $user->update($request->all());

        if ($user->avatar != null) {
            $user->avatar = $_SERVER['REQUEST_SCHEME'] . '://' . $_SERVER['SERVER_NAME'] . '/storage/avatar/' . $user->avatar;
        }

        return response()->json($user, 201);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $user =  User::findOrfail($id);
        $user->delete();
    }

    public function online()
    {
        $date = date_create(date("Y-m-d H:i:s"));
        date_sub($date, date_interval_create_from_date_string('1 minutes'));
        $dateWhere = date_format($date, 'Y-m-d H:i:s');

        $users = DB::table('users')->where('ultimo_acesso', '>', $dateWhere)->get();

        return response()->json($users, 201);
    }
}
