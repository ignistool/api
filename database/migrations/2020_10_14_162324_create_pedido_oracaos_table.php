<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePedidoOracaosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pedido_oracaos', function (Blueprint $table) {
            $table->id();
            $table->string('nome_solicitante')->nullable();
            $table->string('sobrenome_solicitante')->nullable();
            $table->string('telefone_solicitante')->nullable();
            $table->string('email_solicitante')->nullable();
            $table->integer('categoria_pedido_id');
            $table->text('texto');
            $table->boolean('origem_publico')->default('0');
            $table->boolean('publico')->nullable();
            $table->boolean('privado')->nullable();
            $table->boolean('urgente')->nullable();
            $table->boolean('enviado_plantao')->nullable();
            $table->integer('plantao_id')->nullable();
            $table->boolean('retorno')->nullable();
            $table->boolean('triado')->default('0');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pedido_oracaos');
    }
}
