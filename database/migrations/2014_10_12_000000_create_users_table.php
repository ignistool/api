<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->id();

            $table->string('nome')->nullable();
            $table->string('sobrenome')->nullable();
            $table->string('password')->nullable();
            $table->string('email')->unique();
            $table->timestamp('email_verified_at')->nullable();
            $table->string('telefone',15)->nullable();

            $table->integer('conta_id')->nullable();
            $table->integer('tipo_usuario')->nullable();
            $table->integer('funcao_usuario')->nullable();
            
            $table->string('data_nascimento',10)->nullable();
            $table->string('cpf',14)->nullable();

            $table->string('avatar')->nullable();

            $table->string('token_recover_password')->nullable();
            
            $table->dateTime('ultimo_acesso', 0)->nullable();
        

            $table->rememberToken();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
