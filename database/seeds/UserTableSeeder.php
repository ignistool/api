<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            [
                'nome' => 'Renan',
                'sobrenome' => 'Joppert',
                'email' => 'renanljp@gmail.com',
                'telefone' => '(41) 98715-9696',
                'data_nascimento' => '1996-07-30',
                'cpf' => '082.691.439-01',
                'password' => bcrypt('Coala@9676'),
                'funcao_usuario' => 6,
                'tipo_usuario' => 1,
            ],
            [
                'nome' => 'Daniel',
                'sobrenome' => 'Aron Schmitt',
                'email' => 'schmitt.contato@gmail.com',
                'telefone' => '(41) 99503-6787',
                'data_nascimento' => '1996-09-19',
                'cpf' => '084.584.989-10',
                'password' => bcrypt('123456'),
                'funcao_usuario' => 6,
                'tipo_usuario' => 1,
            ]
        ]);
    }
}
