<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class DevocionalTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('devocionals')->insert([ 
            'titulo' => 'DEZEMBRO 2018' ,
            'texto' => 'Prezados irmãos em Cristo!' ,
            'citacao' => '“Também vos digo que, se dois de vós concordarem na terra acerca de qualquer coisa que pedirem, isso lhes será feito por meu Pai, que está nos céus.” — Mateus 18.19' ,
            'editor_id' => '1'
        ]);
    }
}
