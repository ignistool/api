<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class ContaSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('contas')->insert([
            [
                'id' => 1,
                'nome' => 'PIB',
                'logo' => '',
                'cep' => '80.410-020',
                'endereco' => 'Rua Bento Viana, 35',
                'bairro' => 'Batel',
                'cidade' => 'Curitiba',
                'estado' => 'PR',
                'pais' => 'Brasil',
                'cpf_cnpj' => '',
                'inscricao_estadual' => '',
                'razao_social' => '',
                'nome_fantasia' => '',
                'nome_responsavel' => 'Michel',
                'sobrenome_resposavel' => 'Piragibe',
                'telefone_responsavel' => '(41) 3333-3333',
                'email_responsavel' => 'michel.piragibe@pibcuritiba.org.br'
            ]
        ]);
    }
}
