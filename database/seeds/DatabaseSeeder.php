<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call(UserTableSeeder::class); 
        $this->call(CategoriasTableSeeder::class); 
        $this->call(DevocionalTableSeeder::class); 
        $this->call(TemasTableSeeder::class); 
        $this->call(FuncoesUsuarioTableSeeder::class); 
        $this->call(TiposUsuarioTableSeeder::class);
        $this->call(FormSeeder::class);
        $this->call(ContaSeeder::class);
    }
}
