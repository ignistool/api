<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class TemasTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('temas_pedidos')->insert([
            [ 'label' => 'Salvação' ],
            [ 'label' => 'Saúde/Cura' ],
            [ 'label' => 'Finanças/Provisão' ],
            [ 'label' => 'Família/Casamento' ],
            [ 'label' => 'Libertação' ],
            [ 'label' => 'Livramento' ],
            [ 'label' => 'Trabalho/Estudos' ],
            [ 'label' => 'Direcionamento' ],
            [ 'label' => 'Restauração Emocional' ],
            [ 'label' => 'Sexualidade' ],
            [ 'label' => 'Crescimento Espiritual' ],
            [ 'label' => 'Nação' ],
            [ 'label' => 'Missões' ],
            [ 'label' => 'Igreja' ],
            [ 'label' => 'Meu ministério' ],
            [ 'label' => 'Outro' ]
        ]);
    }
}
