<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;


class TiposUsuarioTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('tipos_usuarios')->insert([
            [ 'label' => 'Administrador' ],
            [ 'label' => 'Pastores (bispo, apos)' ],
            [ 'label' => 'Líderes (Ministros, Coord, Sup, líder de cel)' ],
            [ 'label' => 'Missionários (evg, aviva,)' ],
            [ 'label' => 'Funcionários (diretores, secretarias,)' ],
            [ 'label' => 'Voluntários (staff, aux)' ],
            [ 'label' => 'Membros' ],
            [ 'label' => 'Igrejas e Congregaçoes' ],
            [ 'label' => 'Ministérios e Cultos' ],
            [ 'label' => 'Células e redes' ]
        ]);
    }
}
