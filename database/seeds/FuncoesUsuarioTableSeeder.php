<?php

use Illuminate\Database\Seeder;

class FuncoesUsuarioTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('funcoes_usuarios')->insert([
            [ 'nome' => 'Usuário' ],
            [ 'nome' => 'Editor de Devocional' ],
            [ 'nome' => 'Editor de Triagem ' ],
            [ 'nome' => 'Editor Geral' ],
            [ 'nome' => 'Administrador' ],
            [ 'nome' => 'Super Administrador' ]
        ]);
    }
}
