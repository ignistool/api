<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// Route::middleware('auth:api')->get('/user', function (Request $request) {
//     return $request->user();
// });


Route::get('pedidos/triagem', 'Api\PedidoOracaoController@triagem');
Route::get('pedidos/intercessao', 'Api\PedidoOracaoController@intercessao');
Route::get('pedidos/intercessao/{pedidoID}/oracao/{userID}', 'Api\PedidoOracaoController@orar');
Route::get('pedidos/seed', 'Api\PedidoOracaoController@getPedidosSeed');
Route::get('contas/logo', 'Api\ContaController@getLogo');

Route::post('cadastro', 'Api\AuthController@register');     //Cadastro de users na plataforma

Route::apiResource('pedidos/categorias', 'Api\CategoriasPedidoController');
Route::apiResource('pedidos/temas', 'Api\TemasPedidoController');
Route::apiResource('plantoes', 'Api\PlantoesController');
Route::apiResource('contas', 'Api\ContaController');
Route::apiResource('devocional', 'Api\DevocionalController');
Route::apiResource('pedidos', 'Api\PedidoOracaoController');
Route::apiResource('destaques', 'Api\DestaqueController');
Route::apiResource('form', 'Api\FormController');

Route::apiResource('usuarios/tipos', 'Api\TiposUsuarioController');
Route::apiResource('usuarios/funcoes', 'Api\FuncoesUsuarioController');
Route::apiResource('usuarios', 'Api\UserController');

Route::apiResource('emails/templates', 'Api\TemplateEmailController');

Route::post('usuario/recuperar-senha', 'Api\PasswordResetRequestController@sendPasswordResetEmail');
Route::post('usuario/alterar-senha', 'Api\PasswordResetRequestController@ressetPassword');
Route::post('usuarios/alterar-senha-interna', 'Api\PasswordResetRequestController@changePasswordInDashboard');
Route::post('usuarios/funcoes-adm/redefinir-senha', 'Api\PasswordResetRequestController@sendEmailPassword');
Route::get('usuarios-online', 'Api\UserController@online');
Route::post('usuarios/avatar/{id}', 'Api\UserController@changeAvatar');


Route::post('contas/alterar-logo/1', 'Api\ContaController@changeLogo');

Route::get('relatorio/pedidos/ultimos-meses', 'Api\PedidoOracaoController@getReport2Months');

Route::group([

    'middleware' => 'api',
    'prefix' => 'auth'

], function ($router) {

    // Route::post('cadastro', 'AuthController@register');
    Route::post('login', 'Api\AuthController@login');
    Route::post('logout', 'Api\AuthController@logout');
    Route::post('refresh', 'Api\AuthController@refresh');
    Route::get('me', 'Api\AuthController@me');
    // Route::post('alterar-senha', 'Api\AuthController@changePassword');
});
